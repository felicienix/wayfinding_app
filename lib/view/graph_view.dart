/// @file graph_view.dart
/// @author Félicien Fiscus (felicien.fgay@gmail.com)
/// @brief file for the Graph View which displays the current graph loaded
/// in the app.
/// @version 0.1
/// @date 2023-10-20
///
/// @copyright GNU General Public License (GPL) 2023
///
import 'package:flutter/material.dart';
import '../presenter/presenter.dart';

///
/// @brief the view widget
class GraphView extends StatefulWidget {
  Presenter _presenter = Presenter();
  List<VisualNode> nodes = List<VisualNode>.empty(growable: true);
  int graphID = 0;

  GraphView({super.key, this.graphID = 0});

  @override
  State<GraphView> createState() => _GraphView();
}

///
/// @brief the actual graph view widget state
class _GraphView extends State<GraphView> {
  @override
  void initState() {
    super.initState();
  }

  ///
  /// @brief draws the view (for more details, see dart documentation)
  @override
  Widget build(BuildContext context) {
    const textStyle = TextStyle(fontSize: 25);
    const spacerSmall = SizedBox(height: 10);
    int start = widget._presenter.currentPath.isNotEmpty
        ? widget._presenter.currentPath.first
        : 0;
    int end = widget._presenter.currentPath.length > 1
        ? widget._presenter.currentPath.last
        : 0;
    NiceButton loadGraphButton = NiceButton(
      text: 'Load graph ${widget.graphID}',
    );
    loadGraphButton.onPressedFunc = () {
      widget._presenter.getBuildingMapGraph(widget.graphID).then((value) {
        setState(() {
          widget.nodes = value;
        });
      });
    };
    NiceButton computePathButton = NiceButton(
      text: 'Compute path',
    );
    computePathButton.onPressedFunc = () {
      setState(() {
        widget._presenter.updateCurrentPath();
      });
    };

    return Column(
      children: [
        Text(
          'The shortest path between  ${start.toString()} and ${end.toString()} is: ${widget._presenter.currentPath.toString()}',
          style: textStyle,
          textAlign: TextAlign.center,
        ),
        spacerSmall,
        for (var node in widget.nodes)
          Column(
            children: [
              node,
              spacerSmall,
            ],
          ),
        Image.asset('assets/images/graph_testmap${widget.graphID}.png'),
        spacerSmall,
        loadGraphButton,
        spacerSmall,
        computePathButton,
      ],
    );
  }
}

///
/// @brief widget class to have a nice button with a callback displayed on the screen
/// (used by the views)
class NiceButton extends StatelessWidget {
  Function? onPressedFunc;
  String text = '';

  NiceButton({this.text = 'Button'});

  @override
  Widget build(BuildContext context) {
    return ElevatedButton(
      onPressed: () {
        onPressedFunc!();
      },
      style: ElevatedButton.styleFrom(
        backgroundColor: Colors.blue,
        padding: EdgeInsets.symmetric(horizontal: 20, vertical: 10),
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(10.0),
        ),
      ),
      child: Text(
        text,
        style: TextStyle(
          fontSize: 20,
          color: Colors.white,
        ),
      ),
    );
  }
}

///
/// @brief a graphical node that will be displayed on screen
class VisualNode extends StatefulWidget {
  double radius = 20.0;
  int value = 0;
  String neighbors = '{}';
  Function selectNodeCallback = (selected) {};

  VisualNode(
      {Key? key,
      required this.radius,
      required this.value,
      this.neighbors = '{}'})
      : super(key: key);

  @override
  State<VisualNode> createState() => _VisualNode();
}

enum NodeState { notSelected, selected }

///
/// @brief the graphical node state
class _VisualNode extends State<VisualNode> {
  NodeState _state = NodeState.notSelected;

  ///
  /// @brief draws the graphical node, for more info see dart documentation
  @override
  Widget build(BuildContext context) {
    Color color = (_state == NodeState.notSelected) ? Colors.black : Colors.red;

    return GestureDetector(
      onTap: () {
        setState(() {
          _state = (_state == NodeState.notSelected)
              ? NodeState.selected
              : NodeState.notSelected;
          widget.selectNodeCallback(_state);
        });
      },
      child: Row(
        children: [
          CustomPaint(
            size: Size(widget.radius, widget.radius),
            painter: CircleTextPainter(widget.value.toString(), color),
          ),
          Text(widget.neighbors),
        ],
      ),
    );
  }
}

///
///@brief widget representing a circle with a centered text inside of it
/// (used by [VisualNode])
class CircleTextPainter extends CustomPainter {
  final String text;
  Color circleColor;

  CircleTextPainter(this.text, this.circleColor);

  ///
  /// @brief draws the circle and text, for more information, see dart documentation
  @override
  void paint(Canvas canvas, Size size) {
    final paint = Paint()
      ..color = circleColor
      ..style = PaintingStyle.stroke;
    paint.strokeWidth = 1.0;

    final center = Offset(size.width * 0.5, size.height * 0.5);
    final radius = size.width * 0.5;

    canvas.drawCircle(center, radius, paint);

    final textPainter = TextPainter(
      text: TextSpan(
          text: text,
          style: TextStyle(
            color: circleColor,
            fontSize: 10.0,
          )),
      textAlign: TextAlign.center,
      textDirection: TextDirection.ltr,
    );

    textPainter.layout(minWidth: 0, maxWidth: size.width);
    final textWidth = textPainter.width;
    final textHeight = textPainter.height;

    final textOffset =
        Offset(center.dx - textWidth * 0.5, center.dy - textHeight * 0.5);

    textPainter.paint(canvas, textOffset);
  }

  ///
  /// @brief whether, when used in a stateful object, it should repaint when the
  /// state of the object is changed. For more information, see dart documentation
  @override
  bool shouldRepaint(CustomPainter oldDelegate) {
    return true;
  }
}
