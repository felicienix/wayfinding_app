/// @file main.dart
/// @author Félicien Fiscus (felicien.fgay@gmail.com)
/// @brief file for the main view of the app, and the main function
/// @version 0.1
/// @date 2023-10-20
///
/// @copyright GNU General Public License (GPL) 2023
///
import 'package:flutter/material.dart';
import 'package:wayfinding_app/presenter/presenter.dart';
import 'view/graph_view.dart';

void main() {
  runApp(const WayfindingApp());
}

///
/// @brief the main view of the app
class WayfindingApp extends StatefulWidget {
  const WayfindingApp({super.key});

  @override
  State<WayfindingApp> createState() => _WayfindingApp();
}

///
/// @brief the main view's state of the app
class _WayfindingApp extends State<WayfindingApp> {
  final Presenter _presenter = Presenter();

  /// this is temporary as there should only be one graph view
  /// at a time, it is just here to demonstrate the features
  GraphView graph0 = GraphView(
    key: Key('graph_0'),
    graphID: 0,
  );
  GraphView graph1 = GraphView(
    key: Key('graph_1'),
    graphID: 1,
  );

  @override
  void initState() {
    super.initState();
  }

  ///
  /// @brief draws the main view, for more information, see dart documentation
  @override
  Widget build(BuildContext context) {
    NiceButton populateButton = NiceButton(text: 'Populate Database');
    populateButton.onPressedFunc = () {
      _presenter.loadDefaultGraphs().then((value) {
        setState(() {});
      });
    };
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: const Text('Wayfinding app'),
        ),
        body: SingleChildScrollView(
          child: Container(
              padding: const EdgeInsets.all(10),
              child: Column(
                children: [
                  populateButton,
                  graph0,
                  graph1,
                ],
              )),
        ),
      ),
    );
  }
}
