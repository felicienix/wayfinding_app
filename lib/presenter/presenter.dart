/// @file presenter.dart
/// @author Félicien Fiscus (felicien.fgay@gmail.com)
/// @brief Presenter class to interface the [WayfindingInteractor] with the Views
/// and user input.
/// @version 0.1
/// @date 2023-10-20
///
/// @copyright GNU General Public License (GPL) 2023
///
import 'package:interactor/interactor.dart';
import 'dart:developer' as dev;
import '../view/graph_view.dart' as graph_stuff;

class Presenter {
  WayFindingInteractor _interactor = WayFindingInteractor();
  List<int> currentPath = List<int>.empty();

  /// nodes selected by the user
  Set<int> _chosenNodes = {};

  Presenter();

  ///
  /// @brief loads a new graph identified by graphID and returns its
  /// list of nodes
  /// @param graphID the ID of the graph to be loaded from the database into the app
  /// @return list of [VisualNode] representing the graph loaded
  Future<List<graph_stuff.VisualNode>> getBuildingMapGraph(int graphID) async {
    var mapGraph = await _interactor.getBuildingMapFromDB(graphID);

    List<graph_stuff.VisualNode> nodeGraph = List.empty(growable: true);
    for (int node = 0; node < mapGraph.length; ++node) {
      graph_stuff.VisualNode addedNode = graph_stuff.VisualNode(
          radius: 20.0, value: node, neighbors: mapGraph[node].toString());
      addedNode.selectNodeCallback = (selected) {
        if (selected == graph_stuff.NodeState.selected)
          _chosenNodes.add(addedNode.value);
        else
          _chosenNodes.remove(addedNode.value);
      };
      nodeGraph.add(addedNode);
    }

    return nodeGraph;
  }

  ///
  /// @brief updates the currentPath list to be the shortest path
  /// between the first and second node in the [_chosenNode] set.
  void updateCurrentPath() {
    /// TODO: replace dev.log by something visible by the user on the app
    if (_chosenNodes.length < 2) {
      dev.log('not enough nodes chosen, choose two nodes exactly');
      return;
    }
    if (_chosenNodes.length > 2) {
      dev.log(
          'too much nodes chosen, please remove by clicking on them, choose two nodes exactly');
      return;
    }
    currentPath =
        _interactor.getShortestPath(_chosenNodes.first, _chosenNodes.last);
  }

  ///
  /// @brief loads the sample graphs (see assets/images/)
  /// into the mongoDB database
  Future<void> loadDefaultGraphs() async {
    return await _interactor.populateSampleGraphs();
  }
}
