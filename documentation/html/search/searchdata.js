var indexSectionsWithContent =
{
  0: "0_bcdefghilmnprsuvw",
  1: "_bcgilmnpvw",
  2: "cd",
  3: "gimnp",
  4: "bcdefghilmnprsu",
  5: "_bgil",
  6: "biv",
  7: "0w"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "namespaces",
  3: "files",
  4: "functions",
  5: "variables",
  6: "typedefs",
  7: "pages"
};

var indexSectionLabels =
{
  0: "All",
  1: "Classes",
  2: "Namespaces",
  3: "Files",
  4: "Functions",
  5: "Variables",
  6: "Typedefs",
  7: "Pages"
};

