///
/// @file graph_view_test.dart
/// @author Félicien Fiscus (felicien.fgay@gmail.com)
/// @brief integration test to check wether the communication between the
/// database and the views (especially the graph view) is working properly
/// @version 0.1
/// @date 2023-10-19
///
/// @copyright GNU General Public License (GPL) 2023
///

import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:wayfinding_app/view/graph_view.dart';
import 'package:wayfinding_app/main.dart';

void main() {
  ///
  /// @brief integration test for the GraphView widget
  ///
  testWidgets('GraphView nodes database import test',
      (WidgetTester tester) async {
    // Build our app and trigger a frame.
    await tester.pumpWidget(const WayfindingApp());

    //find the buttons to communicate with the database
    var populateButton = find.text('Populate Database');
    var loadGraphButton = find.text('Load graph 0');

    //find the graph the test will be on
    var graphViewFound = find.byKey(Key('graph_0'));
    expect(graphViewFound, findsOneWidget);
    final graphView = tester.widget<GraphView>(graphViewFound);

    await tester.tap(populateButton);
    //artificially wait for the database to be populated
    //(5 seconds should be more than enough)
    int i = 0;
    while (++i < 50) {
      await Future.delayed(Duration(milliseconds: 100));
    }
    await tester.tap(loadGraphButton);

    //artificially wait for the database to answer to the load request
    i = 0;
    while (graphView.nodes.isEmpty) {
      await Future.delayed(Duration(milliseconds: 100));
      expect(++i < 100, true); //time out if it takes more than 10 seconds
    }

    final nodes = graphView.nodes;
    //it should be the graph sample 0 (see assets/images/graĥ_testmap0.png)
    expect(nodes.length, 4);
    expect(nodes.elementAt(0).radius, 20.0);
    expect(nodes.elementAt(0).value, 0);
    expect(nodes.elementAt(0).neighbors, '{1: 1, 2: 5}');
    expect(nodes.elementAt(1).radius, 20.0);
    expect(nodes.elementAt(1).value, 1);
    expect(nodes.elementAt(1).neighbors, '{2: 2}');
    expect(nodes.elementAt(2).radius, 20.0);
    expect(nodes.elementAt(2).value, 2);
    expect(nodes.elementAt(2).neighbors, '{}');
    expect(nodes.elementAt(3).radius, 20.0);
    expect(nodes.elementAt(3).value, 3);
    expect(nodes.elementAt(3).neighbors, '{0: 2}');
  });
}
