# Wayfinding app

This project constitutes a slice of a wayfinding app built for the Mobile System Development paper 
at Auckland University of Technology, Auckland New Zealand.

The 'slice' is 2 of the 6 functional requirements listed in the design document which was the first asssignment of the class.
Namely, 
* computing the shortest path between two spacial points in a building,
* downloading a user given building map.

For simplicity reasons, the 'building map' and 'building' are just graphs for now
as the feature of transforming a building map into a graph isn't available yet.

## Documentation

The documentation is hosted locally
and can be opened via the `documentation/html/index.html` file 
using your favorite browser !

The actual code developed here is in two parts :
* [the interactor plugin](interactor/), more precisely : the lib and src directory
* the dart application, more precisely ; the lib and integration_test directories.

### Tests (requires [flutter](#compiling-and-installing-via-usb) installed)

If you want to run the integration tests,
type
```
flutter test integration_test
```
and choose on which device you want to perform the tests.

## Compiling and installing via usb

To install the app, from this source code, on your phone you'll have 
to have [flutter](https://docs.flutter.dev/get-started/install) installed on your system and then run `flutter build --release` in the project directory.
Then connect your phone to your computer while having enabled the [developer mode](https://developer.android.com/studio/debug/dev-options?hl=en) on your phone,
and once your phone is connected, run `flutter install --release` to
install the app (in release mode) on your phone.

## Database

In order for the app to work properly, 
it needs to have access to a mongodb database,
which in this case, needs to be created.

To create your mongodb database instance, 
follow the steps for [installing mongodb](https://www.mongodb.com/docs/manual/administration/install-community/).

Then you have to create the database, collection and user 
for the app to be able to access it.

### Database creation and access

For that you'll need to first go to the [mongod config file](https://www.mongodb.com/docs/manual/reference/configuration-options/) 
and modify the line
```
security:
  authorization: enabled
```
from `enabled` to `disabled`.

Then restart the mongod service, for instance,
on linux it would be the command
```bash
sudo systemctl restart mongod
```

Now connect to mongodb through mongosh 
```bash
mongosh
```

and enter the following command:
```bash
db.createUser({user:"adminUser", pwd:"admin", roles:["root"]})
```

After you've done that you can turn the authorization back on 
by changing back the config file authorization section from 
'disabled' to 'enabled',
and while you're at it, also change the 
network section like this:
```
# network interfaces
net:
  port: 27017
  bindIp: 0.0.0.0#allows connections to come from any device, to be used only to test stuff
  maxIncomingConnections: 20000

(...other things...)

security:
  authorization: enabled
```

Restart the mongod service 
and then go to mongodb again with this command:
```bash
mongosh mongodb://adminUser:admin@127.0.0.1/
```

Once you're in the mongodb shell,
type these command:
```bash
use wayfinding_db
db.createCollection('BuildingMaps')
db.createUser({user:"wayfinding_user", pwd:"pwd0", roles:[{role: "readWrite", db: "wayfinding_db"}]})
```

Once that is done, you just have to modify
the line 56 of the [interactor](interactor/lib/interactor.dart) dart file
to replace the content of the string '_databaseHost' by your IP address.

Then you just have to rebuild the project using `flutter build`
and you are good to go !

## How to use the app

On the app, you have a *populate Database* button

![](documentation/wayfinding_app_capture0.png)

which if you click on it and wait a few seconds, should populate the mongodb
database you've just setup(see [Database](#database) section).

Then you can load the graphs for each of the two sample graphs, by 
tapping on the respective buttons which will, after around 1sec max, 
effectively load the graph on your app.

![](documentation/wayfinding_app_capture1.png)

Then to compute the shortest path between two nodes of a graph,
just select two of the nodes and then click on the button 'Compute path'.
It should then display the shortest path between the two points of the graph.

![](documentation/wayfinding_app_capture2.png)


## Installing without flutter (only on android)

If you just want to manually download and install the app on 
your phone, then you can download the file `release_install/app-release.apk`
on your android phone and then run it with the android installer toolkit.
You will have to [enable installing unknown apps](https://www.groovypost.com/howto/map-a-network-drive-on-mac/#Installing%20Unknown%20Source%20Apps%20on%20Android%2010%20and%20Later) on your phone
before you can actually install the app and your phone will ask 
you if you're sure about it, say yes :).

After following this, you should have the app installed on your phone, congrats !

### WARNING DATABASE ACCESS

If you install the code as it is and not setup your own mongodb [database](#database), then the app will work but will not have access to any database, thus rendering it useless.

That is why I strongly recommend installing flutter to rebuild the project and setup the 
database so that it works on any device.