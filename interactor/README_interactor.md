# Wayfinding backend

The C++ interactor backend with the dart bindings using ffi and ffigen.
If you want to generate the dynamic library file, you can do so by
going in the `src` folder and typing
```bash
cmake .
make
```

## Tests

If you want to generate the c++ unit tests, 
you have to go to the [c++ tests directory](src/cxx_tests/)
and type 
```bash
cmake .
make
```

You can then run them by typing
```bash
./shortestsPathTests
```
in the console.
