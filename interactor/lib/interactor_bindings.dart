// ignore_for_file: always_specify_types
// ignore_for_file: camel_case_types
// ignore_for_file: non_constant_identifier_names

// AUTO GENERATED FILE, DO NOT EDIT.
//
// Generated by `package:ffigen`.
import 'dart:ffi' as ffi;

/// Bindings for `src/c_wrappers/*.h`.
///
/// Regenerate bindings with `flutter pub run ffigen --config ffigen.yaml`.
///
class WayfindingBackendBindings {
  /// Holds the symbol lookup function.
  final ffi.Pointer<T> Function<T extends ffi.NativeType>(String symbolName)
      _lookup;

  /// The symbols are looked up in [dynamicLibrary].
  WayfindingBackendBindings(ffi.DynamicLibrary dynamicLibrary)
      : _lookup = dynamicLibrary.lookup;

  /// The symbols are looked up with [lookup].
  WayfindingBackendBindings.fromLookup(
      ffi.Pointer<T> Function<T extends ffi.NativeType>(String symbolName)
          lookup)
      : _lookup = lookup;

  /// @brief C wrapper for C++ Interactor constructor method
  /// @see Interactor.hxx
  ///
  /// @return abstract C pointer of an Interactor instance allocated on the heap
  ffi.Pointer<Interactor> getNewInteractor() {
    return _getNewInteractor();
  }

  late final _getNewInteractorPtr =
      _lookup<ffi.NativeFunction<ffi.Pointer<Interactor> Function()>>(
          'getNewInteractor');
  late final _getNewInteractor =
      _getNewInteractorPtr.asFunction<ffi.Pointer<Interactor> Function()>();

  /// @brief C wrapper for freeing an Interactor object created by the getNewInteractor function
  /// @see Interactor.hxx
  ///
  /// @param interactor abstract C pointer of an Interactor instance allocated on the heap
  void freeInteractor(
    ffi.Pointer<Interactor> interactor,
  ) {
    return _freeInteractor(
      interactor,
    );
  }

  late final _freeInteractorPtr =
      _lookup<ffi.NativeFunction<ffi.Void Function(ffi.Pointer<Interactor>)>>(
          'freeInteractor');
  late final _freeInteractor =
      _freeInteractorPtr.asFunction<void Function(ffi.Pointer<Interactor>)>();

  /// @brief C wrapper for C++ Interactor generateSampleMapGraph method
  /// @see Interactor.hxx
  ///
  /// @param interactor abstract C pointer of an Interactor instance allocated on the heap
  void generateSampleMapGraph(
    ffi.Pointer<Interactor> interactor,
  ) {
    return _generateSampleMapGraph(
      interactor,
    );
  }

  late final _generateSampleMapGraphPtr =
      _lookup<ffi.NativeFunction<ffi.Void Function(ffi.Pointer<Interactor>)>>(
          'generateSampleMapGraph');
  late final _generateSampleMapGraph = _generateSampleMapGraphPtr
      .asFunction<void Function(ffi.Pointer<Interactor>)>();

  /// @brief C wrapper for C++ Interactor generateMapGraph method
  /// @see Interactor.hxx
  ///
  /// @param interactor abstract C pointer of an Interactor instance allocated on the heap
  void generateMapGraph(
    ffi.Pointer<Interactor> interactor,
    BasicArray adjacencyList,
  ) {
    return _generateMapGraph(
      interactor,
      adjacencyList,
    );
  }

  late final _generateMapGraphPtr = _lookup<
      ffi.NativeFunction<
          ffi.Void Function(
              ffi.Pointer<Interactor>, BasicArray)>>('generateMapGraph');
  late final _generateMapGraph = _generateMapGraphPtr
      .asFunction<void Function(ffi.Pointer<Interactor>, BasicArray)>();

  /// @brief C wrapper for C++ Interactor shortestPath method
  /// @see Interactor.hxx
  ///
  /// @param interactor abstract C pointer of an Interactor instance allocated on the heap
  ffi.Pointer<ffi.Int32> getShortestPath(
    ffi.Pointer<Interactor> interactor,
    int a,
    int b,
    ffi.Pointer<ffi.Int32> pathSize,
  ) {
    return _getShortestPath(
      interactor,
      a,
      b,
      pathSize,
    );
  }

  late final _getShortestPathPtr = _lookup<
      ffi.NativeFunction<
          ffi.Pointer<ffi.Int32> Function(ffi.Pointer<Interactor>, ffi.Int32,
              ffi.Int32, ffi.Pointer<ffi.Int32>)>>('getShortestPath');
  late final _getShortestPath = _getShortestPathPtr.asFunction<
      ffi.Pointer<ffi.Int32> Function(
          ffi.Pointer<Interactor>, int, int, ffi.Pointer<ffi.Int32>)>();
}

final class Interactor extends ffi.Opaque {}

/// @brief utility structure for passing adjacency list from mongoDB from dart to C++
final class BasicArray extends ffi.Struct {
  @ffi.UnsignedInt()
  external int length;

  external ffi.Pointer<ffi.Void> array;
}

/// @brief utility structure for having a pair of the same values, used for storing node id and weight for the adjacency list
final class ValuePair extends ffi.Struct {
  @ffi.Int32()
  external int first;

  @ffi.UnsignedInt()
  external int second;
}
