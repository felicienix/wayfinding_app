/// @file interactor.dart
/// @author Félicien Fiscus (felicien.fgay@gmail.com)
/// @brief this file loads the C++ dynamic library, provides a
/// high level dart object(interactor) to call C++ functions
/// and communicate with the mongoDB database.
/// @version 0.1
/// @date 2023-10-19
///
/// @copyright GNU General Public License (GPL) 2023
///

import 'dart:ffi';
import 'dart:io';
import 'dart:typed_data';
import 'dart:developer' as dev;
import 'package:ffi/ffi.dart';
import 'package:mongo_dart/mongo_dart.dart';

import 'package:flutter/foundation.dart';

/// The bindings for the C types (like [Interactor] or [BasicArray])
import 'interactor_bindings.dart' as interactor_bindings;

const String _libName = 'interactor';

/// The dynamic library in which the symbols for [WayfindingBackendBindings] can be found.
final DynamicLibrary _dylib = () {
  if (Platform.isMacOS || Platform.isIOS) {
    return DynamicLibrary.open('$_libName.framework/$_libName');
  }
  if (Platform.isAndroid || Platform.isLinux) {
    return DynamicLibrary.open('lib$_libName.so');
  }
  if (Platform.isWindows) {
    return DynamicLibrary.open('$_libName.dll');
  }
  throw UnsupportedError('Unknown platform: ${Platform.operatingSystem}');
}();

/// The bindings to the native functions in [_dylib].
final interactor_bindings.WayfindingBackendBindings interactorBindings =
    interactor_bindings.WayfindingBackendBindings(_dylib);

///
/// interactor dart class, providing an interface to the C++ type
/// [Interactor] and communicating with the mongoDB database.
/// @see Interactor.hxx
class WayFindingInteractor {
  late final Pointer<interactor_bindings.Interactor> _interactor;
  static const String _databaseUser = "wayfinding_user";

  ///DO NOT DO THIS
  static const String _databasePWD = "pwd0";
  //////////////
  /// to replace with your own IP address or 10.0.2.2 if running on flutter emulator
  static const String _databaseHost = "192.192.192.192"; //"10.0.2.2";
  static const String _databaseName = "wayfinding_db";

  WayFindingInteractor() : _interactor = interactorBindings.getNewInteractor();

  ///
  /// @brief main feature of the application, calls to the low-level shortest path
  /// computation function and convert it to a dart list
  /// @param start the starting point in the current graph
  /// @param end where you want to go to
  /// @return the shortest path(according to the graph's weights)
  /// in the format [0, 1, 3, 2](here, example of a shortest path between 0(start) and 2(end))
  Int32List getShortestPath(int start, int end) {
    Pointer<Int32> pathSizeBuff = malloc.call();
    Pointer<Int32> path = interactorBindings.getShortestPath(
        _interactor, start, end, pathSizeBuff);
    int pathSize = pathSizeBuff.value;
    malloc.free(pathSizeBuff);

    return path.asTypedList(pathSize);
  }

  ///
  /// @brief converts a adjacency list from the database to a
  /// basic array which is used to generate the inner c++ map structure
  ///
  /// @param dbAdjaList the adjacency list from the DB, needs to be a list
  /// of pairs [{n1: w1, ...}, {n4 : w4}, ...]
  /// with ni being a node of id 'i' and wi is the corresponding weight
  /// to this neighbor node. The first element of the list is the neighbors
  /// of the node 0, the second is for the node 1, etc.
  ///
  /// @return the basicArray adjacency list converted and a list to all the memory addresses pointing to heap allocated
  /// memory that needs freeing and
  (interactor_bindings.BasicArray, List<Pointer<Void>>)
      _convertDatabaseAdjacencyListToBasicArray(List dbAdjaList) {
    //This is so that afterwards the data can be freed without causing memory leaks
    List<Pointer<Void>> memoryAdressesNeedingFreeing =
        List.empty(growable: true);
    //first create the initial BasicArray
    var adjacencyListRef = calloc.call<interactor_bindings.BasicArray>();
    memoryAdressesNeedingFreeing.add(adjacencyListRef.cast());
    interactor_bindings.BasicArray adjacencyList = adjacencyListRef.ref;
    adjacencyList.length = dbAdjaList.length;

    //then create the array of BasicArrays representing the adjacency list
    var basicArrayArray =
        calloc.call<interactor_bindings.BasicArray>(adjacencyList.length);
    memoryAdressesNeedingFreeing.add(basicArrayArray.cast());

    //then we go through each node in the list and populate the
    // just created adjacency list (aka BasicArray)
    for (int node = 0; node < dbAdjaList.length; ++node) {
      //get the neighbors from the database list
      Map neighborsMap = dbAdjaList[node];
      basicArrayArray[node].length = neighborsMap.length;

      //then create the neighbors array of ValuePair for the node we're at
      Pointer<interactor_bindings.ValuePair> neighbors =
          calloc.call(neighborsMap.length);
      memoryAdressesNeedingFreeing.add(neighbors.cast());

      //then for each pair neighbor : weight in the map,
      // populate the array of neighbors with the Valuepair (neighbor, weight)
      int neighbor = 0;
      for (var element in neighborsMap.entries) {
        neighbors[neighbor].first = int.parse(element.key);
        neighbors[neighbor].second = element.value;
        neighbor++;
      }
      //and then add the neibhbors array to the corresponding node slot
      basicArrayArray[node].length = neighborsMap.length;
      basicArrayArray[node].array = neighbors.cast();
    }

    //finally add the now populated basicArrayArray to the final adjacency list
    //and return the list of memory addresses allocated for this conversion to happen
    adjacencyList.array = basicArrayArray.cast();
    return (adjacencyList, memoryAdressesNeedingFreeing);
  }

  ///
  /// @brief utility function to insert sample graphs
  /// into the mongoDB database for the app
  /// to be tested on. IF THE DATABASE IS NOT EMPTY, IT WILL REMOVE
  /// ALL GRAPHS THAT ARE STORED IN THE DATABASE BEFORE INSERTING THE
  /// TWO SAMPLE GRAPHS.
  /// @note the two graphs are displayed in assets/images
  Future<void> populateSampleGraphs() async {
    dev.log('starting to populate the sample graphs to the database');
    var db = await Db.create(
        "mongodb://$_databaseUser:$_databasePWD@$_databaseHost/$_databaseName");

    await db.open();

    var buildingMaps = db.collection('BuildingMaps');
    //if there already are graphs in the database, remove everything first
    if ((await buildingMaps.count(where.exists('graphID'))) != 0) {
      await buildingMaps.deleteMany(where.exists('graphID'));
    }
    //insert the two sample graphs
    await buildingMaps.insertMany([
      {
        'graphID': 0,
        'adjacencyList': [
          {'1': 1, '2': 5},
          {'2': 2},
          {},
          {'0': 2}
        ]
      },
      {
        'graphID': 1,
        'adjacencyList': [
          {'2': 2, '3': 1, '4': 5},
          {'3': 2, '4': 3},
          {'0': 2, '5': 3},
          {'0': 1, '1': 2, '5': 4, '8': 1},
          {'0': 5, '1': 3},
          {'2': 3, '3': 4, '6': 1, '7': 7, '9': 6},
          {'5': 1, '9': 4},
          {'5': 7, '8': 3},
          {'3': 1, '7': 3},
          {'5': 6, '6': 4}
        ]
      }
    ]);

    await db.close();

    dev.log('finished populating the database with sample graphs');
  }

  ///
  /// @brief high level getter for a graph in the database.
  /// Also updates the current graph stored in the low-level interactor to be
  /// the one loaded from the database.
  /// @param graphID id of a graph stored in database, temporary way to
  /// identify a database graph (not reliable for now).
  /// @return the adjacency list corresponding to the graphID value.
  /// The list has the following format [{1: 3}, {}, {1: 2, 0: 1}]
  /// when the graph has 3 nodes (0, 1, 2) and 0 has one neighbor (1) of weight
  /// 3, 1 has 0 neighbors and 2 has 2 neighbors (1 and 0) of weight 2 and 1
  /// respectively.
  Future<List> getBuildingMapFromDB(int graphID) async {
    var db = await Db.create(
        "mongodb://$_databaseUser:$_databasePWD@$_databaseHost:27017/$_databaseName");

    await db.open();

    var buildingMaps = db.collection('BuildingMaps');
    var graph = await buildingMaps.findOne(where.eq('graphID', graphID));
    dev.log('building map loaded from database');
    if (graph != null) {
      //first convert the adjacency list to a basic Array adjacency list
      //so that it is readable for the C bindings
      var (basicArrayAdjacencyList, pointerListNeedingFreeing) =
          _convertDatabaseAdjacencyListToBasicArray(graph['adjacencyList']);
      dev.log('database adjacency list converted to BasicArray');
      //then generate the new map graph in the low-level interactor using the adjacency BasicArray
      interactorBindings.generateMapGraph(_interactor, basicArrayAdjacencyList);
      dev.log('new map graph generated in interactor');
      //and then free all the memory used for the conversion
      for (var pointer in pointerListNeedingFreeing) {
        calloc.free(pointer);
      }
      dev.log('finished freeing everything');
    }

    await db.close();

    if (graph != null) return graph['adjacencyList'];

    return List.empty();
  }
}
