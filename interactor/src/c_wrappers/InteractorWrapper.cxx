#include "InteractorWrapper.h"
#include "Interactor.hxx"
#include <stdio.h>

extern "C"
{

Interactor *getNewInteractor()
{
    return new Interactor();
}

void freeInteractor(Interactor *interactor)
{
    if(interactor)
        delete interactor;
}

void generateSampleMapGraph(Interactor *interactor)
{
    interactor->generateSampleMapGraph();
}

void generateMapGraph(Interactor *interactor, BasicArray adjacencyList)
{
    interactor->generateMapGraph(adjacencyList);
}

int32_t* getShortestPath(Interactor* interactor, int32_t a, int32_t b, int32_t* pathSize)
{
    return interactor->shortestPath(a, b, pathSize);
}

}
