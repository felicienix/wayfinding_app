/**
 * @file InteractorWrapper.h
 * @author Félicien Fiscus (felicien.fgay@gmail.com)
 * @brief C++ to C Wrapper file for the Interactor C++ class to be used by Dart FFI package
 * @version 0.1
 * @date 2023-10-08
 * 
 * @copyright GNU General Public License (GPL) 2023
 * 
 */
#ifndef INTERACTOR_WRAPPER_H
#define INTERACTOR_WRAPPER_H
#include <stdint.h>

#ifdef __cplusplus
extern "C"{
#endif

/**
 * @brief C wrapper abstract type for the C++ Interactor class
 * @see Interactor.hxx
 * 
 */
typedef struct Interactor Interactor;

/**
 * @brief utility structure for passing adjacency list from mongoDB from dart to C++
 * 
 */
typedef struct BasicArray
{
    unsigned int length;
    void* array;
} BasicArray;

/**
 * @brief utility structure for having a pair of the same values, used for storing node id and weight for the adjacency list
 * 
 */
typedef struct ValuePair
{
    int32_t first;
    unsigned int second;
} ValuePair;

/**
 * @brief C wrapper for C++ Interactor constructor method 
 * @see Interactor.hxx
 * 
 * @return abstract C pointer of an Interactor instance allocated on the heap
 */
Interactor* getNewInteractor();

/**
 * @brief C wrapper for freeing an Interactor object created by the getNewInteractor function 
 * @see Interactor.hxx
 * 
 * @param interactor abstract C pointer of an Interactor instance allocated on the heap
 */
void freeInteractor(Interactor* interactor);

/**
 * @brief C wrapper for C++ Interactor generateSampleMapGraph method 
 * @see Interactor.hxx
 * 
 * @param interactor abstract C pointer of an Interactor instance allocated on the heap
 */
void generateSampleMapGraph(Interactor* interactor);

/**
 * @brief C wrapper for C++ Interactor generateMapGraph method 
 * @see Interactor.hxx
 * 
 * @param interactor abstract C pointer of an Interactor instance allocated on the heap
 */
void generateMapGraph(Interactor* interactor, BasicArray adjacencyList);

/**
 * @brief C wrapper for C++ Interactor shortestPath method 
 * @see Interactor.hxx 
 * 
 * @param interactor abstract C pointer of an Interactor instance allocated on the heap
 */
int32_t* getShortestPath(Interactor* interactor, int32_t a, int32_t b, int32_t* pathSize);


#ifdef __cplusplus
}
#endif

#endif //INTERACTOR_WRAPPER_H