#include "Map.hxx"
#include "Node.hxx"
#include "math.h"
#include "stdio.h"
#include "../c_wrappers/InteractorWrapper.h"


Map::Map()
    :graph(), graphSize(0)
{
}

Map::Map(u_int32_t **adjacencyMatrix, size_t matrixSize)
    :graph(new GraphNode[matrixSize]), graphSize(matrixSize)
{
    for(size_t node = 0;node<matrixSize;++node)
        graph[node].nodeID = node;

    for(size_t node = 0;node<matrixSize;++node)
    { 
        for(size_t col = 0;col<matrixSize;++col)
        {
            u_int32_t weightValue = adjacencyMatrix[node][col];
            if(weightValue > 0)
                graph[node].neighbors.pushElementFront(GraphNeighbor{(int32_t)col, weightValue});
        }
    }
}

Map::Map(BasicArray _adjacencyList)
    :graph(new GraphNode[_adjacencyList.length]), graphSize(_adjacencyList.length)
{
    BasicArray* adjacencyList = (BasicArray*)_adjacencyList.array;
    for(size_t node = 0;node<_adjacencyList.length;++node)
    {
        ValuePair* neighbors = (ValuePair*)adjacencyList[node].array;
        for(size_t nodeNeighbor = 0;nodeNeighbor<adjacencyList[node].length;++nodeNeighbor)
        {
            //the first number is the node ID and the second is the weight
            graph[node].neighbors.pushElementFront(GraphNeighbor{neighbors[nodeNeighbor].first, neighbors[nodeNeighbor].second});
        }
    }
}

Map::~Map()
{
    if(graph)
        delete[] graph;
}

size_t Map::getNumberOfNodes() const
{
    return graphSize;
}

void Map::buildGraph(u_int32_t **adjacencyMatrix, size_t matrixSize)
{
    if(graph)
        delete[] graph;

    graph = new GraphNode[matrixSize];
    graphSize = matrixSize;

    for(size_t node = 0;node<matrixSize;++node)
        graph[node].nodeID = node;

    for(size_t node = 0;node<matrixSize;++node)
    { 
        for(size_t col = 0;col<matrixSize;++col)
        {
            u_int32_t weightValue = adjacencyMatrix[node][col];
            if(weightValue > 0)
                graph[node].neighbors.pushElementFront(GraphNeighbor{(int32_t)col, weightValue});
        }
    }
}

void Map::buildGraph(BasicArray _adjacencyList)
{

    if(graph)
        delete[] graph;

    graph = new GraphNode[_adjacencyList.length];
    graphSize = _adjacencyList.length;

    BasicArray* adjacencyList = (BasicArray*)_adjacencyList.array;
    for(size_t node = 0;node<_adjacencyList.length;++node)
    {
        ValuePair* neighbors = (ValuePair*)adjacencyList[node].array;
        for(size_t nodeNeighbor = 0;nodeNeighbor<adjacencyList[node].length;++nodeNeighbor)
        {
            //the first number is the node ID and the second is the weight
            graph[node].neighbors.pushElementFront(GraphNeighbor{neighbors[nodeNeighbor].first, neighbors[nodeNeighbor].second});
        }
    }
}

void Map::computeShortestPath(int32_t start, int32_t end, NodeList<int32_t>& finalPath)
{
    using node = int32_t;
    if(!graph)
        return;
    if(start == end)
    {
        finalPath.pushElementFront(start);
        return;
    }
    //Implentation of A*'s algorithm (based on pseudo code in https://en.wikipedia.org/wiki/A*_search_algorithm)

    //supposed to return an estimate of the cost of going from 'node' to 'end'
    auto heuristic = [](int32_t _node){return 0.f;}; //here it makes the algorithm equivalent to dijkstra's

    NodeList<node> discoveredNodes(start);

    for(size_t i = 0;i<graphSize;++i)
    {
        graph[i].localGoal = INFINITY;
        graph[i].globalGoal = INFINITY;
    }
    graph[start].localGoal = 0.f;
    graph[start].globalGoal = heuristic(start);

    while(!discoveredNodes.empty())
    {
        node current = discoveredNodes.findMinimum(graphCompareFunction)->value;
        if(current == end)
        {
            finalPath.pushElementFront(end);
            for(node parent = graph[end].parentNode; parent != start; parent = graph[parent].parentNode)
            {
                finalPath.pushElementFront(parent);
            }
            finalPath.pushElementFront(start);
            return;
        }

        (void)discoveredNodes.deleteNode(current);

        for(auto neighborNode = graph[current].neighbors.frontNode(); neighborNode != nullptr; neighborNode = neighborNode->next)
        {
            //first compute the local goal if we were to pass through this neighbour
            float potentialLocalGoal = graph[current].localGoal + neighborNode->value.weight;
            if(potentialLocalGoal < graph[neighborNode->value.neighborID].localGoal)
            {
                //passing through the current node to go to this neighbour is the better way to go !
                graph[neighborNode->value.neighborID].parentNode = current;
                graph[neighborNode->value.neighborID].localGoal = potentialLocalGoal;
                graph[neighborNode->value.neighborID].globalGoal = potentialLocalGoal + heuristic(neighborNode->value.neighborID);

                //this neighbor has been discovered, so add it if it's not already in the list
                if(!discoveredNodes.findElement(neighborNode->value.neighborID))
                    (void)discoveredNodes.pushElementFront(neighborNode->value.neighborID);
            }
        }
    }

    printf("ComputeShortestPath didn't find a path\n");
}

GraphNode::GraphNode()
    :nodeID(0), neighbors()
{
}

GraphNode::GraphNode(int32_t _value)
    :nodeID(_value), neighbors()
{
}

GraphNode::~GraphNode()
{
}
