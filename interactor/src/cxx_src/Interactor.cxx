#include "Interactor.hxx"
#include "InteractorWrapper.h"
#include <stdlib.h>

Interactor::Interactor()
{
}

Interactor::~Interactor()
{
}

void Interactor::generateSampleMapGraph()
{

        u_int32_t graph_nondirectional_row0[9] =
            {
                0, 0, 2, 1, 5, 0, 0, 0, 0
            };
        u_int32_t graph_nondirectional_row1[9] =
            {
                0, 0, 0, 2, 3, 0, 0, 0, 0
            };
        u_int32_t graph_nondirectional_row2[9] =
            {
                2, 0, 0, 0, 0, 0, 0, 0, 0
            };
        u_int32_t graph_nondirectional_row3[9] =
            {
                1, 2, 0, 0, 0, 4, 0, 0, 1
            };
        u_int32_t graph_nondirectional_row4[9] =
            {
                5, 3, 0, 0, 0, 0, 0, 0, 0
            };
        u_int32_t graph_nondirectional_row5[9] =
            {
                0, 0, 0, 4, 0, 0, 1, 7, 0
            };
        u_int32_t graph_nondirectional_row6[9] =
            {
                0, 0, 0, 0, 0, 1, 0, 0, 0
            };
        u_int32_t graph_nondirectional_row7[9] =
            {
                0, 0, 0, 0, 0, 7, 0, 0, 3
            };
        u_int32_t graph_nondirectional_row8[9] =
            {
                0, 0, 0, 1, 0, 0, 0, 3, 0
            };

        u_int32_t* graph_nondirectional[9] = {
            graph_nondirectional_row0,
            graph_nondirectional_row1,
            graph_nondirectional_row2,
            graph_nondirectional_row3,
            graph_nondirectional_row4,
            graph_nondirectional_row5,
            graph_nondirectional_row6,
            graph_nondirectional_row7,
            graph_nondirectional_row8,
        };

    currentBuildingMap.buildGraph(graph_nondirectional, 9);
}

void Interactor::generateMapGraph(BasicArray adjacencyList)
{
    currentBuildingMap.buildGraph(adjacencyList);
}

int32_t* Interactor::shortestPath(int32_t a, int32_t b, int32_t* pathSize)
{
    NodeList<int32_t> path;
    int32_t* finalPath;
    currentBuildingMap.computeShortestPath(a, b, path);
    finalPath = new int32_t[path.size()];
    size_t i = 0;
    *pathSize = path.size();
    for(auto current = path.frontNode();current != nullptr;current = current->next)
    {
        finalPath[i++] = current->value;   
    }
    
    return finalPath;
}
