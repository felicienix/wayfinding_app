# The Flutter tooling requires that developers have CMake 3.10 or later
# installed. You should not increase this version, as doing so will cause
# the plugin to fail to compile for some customers of the plugin.
cmake_minimum_required(VERSION 3.10)


project(interactor_library VERSION 0.0.1 LANGUAGES CXX)


include_directories(
  cxx_headers c_wrappers
)

add_library(interactor SHARED
  c_wrappers/InteractorWrapper.cxx cxx_src/Interactor.cxx cxx_src/Map.cxx
)

set_target_properties(interactor PROPERTIES
  PUBLIC_HEADER c_wrappers/InteractorWrapper.h
  OUTPUT_NAME "interactor"
)


target_compile_definitions(interactor PUBLIC DART_SHARED_LIB)
