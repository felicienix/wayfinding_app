/**
 * @file NodeTests.h
 * @author Félicien Fiscus (felicien.fgay@gmail.com)
 * @brief unit test file for the Node C++ class
 * @see Node.hxx
 * @version 0.1
 * @date 2023-10-20
 * 
 * @copyright GNU General Public License (GPL) 2023
 * 
 */
#ifndef NODE_TESTS_HXX
#define NODE_TESTS_HXX
#include "cxxtest/TestSuite.h"
#include "Node.hxx"

class NodeTests : public CxxTest::TestSuite
{
public:
    void testAddAndDelete()
    {
        TS_TRACE("Starting test Add and Delete");

        NodeList<int32_t> linkedList1;
        NodeList<int32_t> linkedList2(25);

        TS_ASSERT_EQUALS(linkedList2.size(), 1);
        TS_ASSERT_EQUALS(linkedList1.size(), 0);
        TS_ASSERT_EQUALS(linkedList2.popFront()->value, 25);
        TS_ASSERT_EQUALS(linkedList2.size(), 0);
        
        linkedList1.pushElementFront(0);
        linkedList1.pushElementFront(3);
        linkedList1.pushElementFront(5);
        linkedList1.pushElementFront(7);
        linkedList1.pushElementFront(5);
        
        linkedList2.pushElementFront(linkedList1.frontNode()->value);
        TS_ASSERT_EQUALS(linkedList1.size(), 5);
        TS_ASSERT_EQUALS(linkedList2.size(), 1);

        TS_ASSERT_EQUALS(linkedList1.frontNode()->value, 5);
        TS_ASSERT_EQUALS(linkedList2.frontNode()->value, 5);
        TS_ASSERT(linkedList1.findNode(linkedList1.frontNode()) == linkedList1.frontNode());
        TS_ASSERT(linkedList1.findElement(7) != nullptr);
        TS_ASSERT(linkedList1.findNode(linkedList1.findElement(7)) == linkedList1.findElement(7));
        TS_ASSERT(linkedList1.findElement(5) != nullptr);
        TS_ASSERT(linkedList1.findNode(linkedList1.findElement(5)) == linkedList1.findElement(5));

        TS_ASSERT(linkedList1.findNode(linkedList1.removeNode(3)) == nullptr);
        (void)linkedList1.deleteNode(7);
        TS_ASSERT(linkedList1.findElement(7) == nullptr);
        (void)linkedList1.deleteNode(5);
        TS_ASSERT(linkedList1.findElement(5) != nullptr);

        NodeList<int32_t> linkedList3(7, 0, 3, 5, 7, 5, 9, 17);

        TS_ASSERT_EQUALS(linkedList3.size(), 7);

        TS_ASSERT_EQUALS(linkedList3.frontNode()->value, 17);

        TS_ASSERT(linkedList3.findNode(linkedList3.frontNode()) == linkedList3.frontNode());
        TS_ASSERT(linkedList3.findElement(7) != nullptr);
        TS_ASSERT(linkedList3.findNode(linkedList3.findElement(7)) == linkedList3.findElement(7));
        TS_ASSERT(linkedList3.findElement(5) != nullptr);
        TS_ASSERT(linkedList3.findNode(linkedList3.findElement(5)) == linkedList3.findElement(5));

        TS_ASSERT(linkedList3.findNode(linkedList3.removeNode(3)) == nullptr);
        (void)linkedList3.deleteNode(7);
        TS_ASSERT(linkedList3.findElement(7) == nullptr);
        (void)linkedList3.deleteNode(5);
        TS_ASSERT(linkedList3.findElement(5) != nullptr);

        TS_TRACE("Ending test Add and Delete");
    }

    void testSortAndIterate()
    {
        TS_TRACE("Starting test Sort and Iterate");

        NodeList<int32_t> inputLinkedList(11, 37, 0, 1, 5, 3, 8, 1, 9, 26, 17, 14);
        NodeList<int32_t> inputLinkedList1(11, 37, 0, 1, 5, 3, 8, 1, 9, 26, 17, 14);
        NodeList<int32_t> inputLinkedList2(11, 0, 37, 1, 3, 8, 5, 9, 1, 26, 14, 17);

        TS_ASSERT(inputLinkedList1.clone().iterate([](int32_t& value){value = 100;}).isEqualTo(NodeList<int32_t>(11, 100)));
        TS_ASSERT(inputLinkedList.isEqualTo(inputLinkedList1));
        
        TS_ASSERT(inputLinkedList.hasTheSameElementsAs(inputLinkedList1));
        
        TS_ASSERT(inputLinkedList.hasTheSameElementsAs(inputLinkedList2));
        
        TS_ASSERT(!inputLinkedList.isEqualTo(inputLinkedList2));

        NodeList<int32_t> outputLinkedList(11, 37, 26, 17, 14, 9, 8, 5, 3, 1, 1, 0);

        TS_ASSERT(inputLinkedList.sort().isEqualTo(outputLinkedList));
        


        TS_TRACE("Ending test Sort and Iterate");
    }
};


#endif //NODE_TESTS_HXX