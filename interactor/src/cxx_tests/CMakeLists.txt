find_package(CxxTest)
if(CXXTEST_FOUND)
  include_directories(${CXXTEST_INCLUDE_DIR})
  enable_testing()

    include_directories(
        ${CMAKE_CURRENT_SOURCE_DIR}/../cxx_headers
    )
  CXXTEST_ADD_TEST(shortestPathTests shortestPathTests.cc 
                  ${CMAKE_CURRENT_SOURCE_DIR}/../cxx_src/Map.cxx
                  ${CMAKE_CURRENT_SOURCE_DIR}/NodeTests.h
                  ${CMAKE_CURRENT_SOURCE_DIR}/MapTests.h)


endif()