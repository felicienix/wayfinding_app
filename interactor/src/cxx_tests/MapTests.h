/**
 * @file MapTests.h
 * @author Félicien Fiscus (felicien.fgay@gmail.com)
 * @brief unit test file for the Map C++ class
 * @see Map.hxx
 * @version 0.1
 * @date 2023-10-20
 * 
 * @copyright GNU General Public License (GPL) 2023
 * 
 */
#ifndef MAP_TESTS_HXX
#define MAP_TESTS_HXX
#include "cxxtest/TestSuite.h"
#include "Map.hxx"
#include "../c_wrappers/InteractorWrapper.h"

class MapTests : public CxxTest::TestSuite
{
public:

    void testMapConstructorMatrix()
    {
        TS_TRACE("Starting test map constructor matrix");

        u_int32_t graph_0_0[2] =
            {
                0, 0
            };
        u_int32_t graph_0_1[2] =
            {
                0, 0
            };
        u_int32_t* graph_0[2] = {graph_0_0, graph_0_1};

        u_int32_t graph_1_0[3] =
            {
                0, 1, 2 
            };
        u_int32_t graph_1_1[3] =
            {
                1, 0, 3
            };
        u_int32_t graph_1_2[3] =
            {
                2, 3, 0
            };
        u_int32_t* graph_1[3] = {graph_1_0, graph_1_1, graph_1_2};

        u_int32_t graph_2_0[5] = 
            {
                0, 1, 2, 5, 9
            };
        u_int32_t graph_2_1[5] = 
            {
                1, 0, 0, 0, 0 
            };
        u_int32_t graph_2_2[5] = 
            {
                2, 0, 0, 3, 21
            };
        u_int32_t graph_2_3[5] = 
            {
                5, 0, 15, 0, 7
            };
        u_int32_t graph_2_4[5] = 
            {
                13, 3, 17, 7, 0
            };
        u_int32_t* graph_2[5] = {graph_2_0, graph_2_1, graph_2_2, graph_2_3, graph_2_4};
        
        Map map0{graph_0, 2U};
        Map map1{graph_1, 3U};
        Map map2{graph_2, 5U};
        
        //test for map 0
        TS_ASSERT_EQUALS(map0.graph[0].neighbors.size(), 0U);
        TS_ASSERT_EQUALS(map0.graph[1].neighbors.size(), 0U);

        //test for map 1
        TS_ASSERT_EQUALS(map1.graph[0].neighbors.size(), 2);
        TS_ASSERT_EQUALS(map1.graph[1].neighbors.size(), 2);
        TS_ASSERT_EQUALS(map1.graph[2].neighbors.size(), 2);

        TS_ASSERT(map1.graph[0].neighbors.hasTheSameElementsAs(NodeList<GraphNeighbor>(2, (GraphNeighbor){1, 1}, (GraphNeighbor){2, 2})));
        TS_ASSERT(map1.graph[1].neighbors.hasTheSameElementsAs(NodeList<GraphNeighbor>(2, (GraphNeighbor){0, 1}, (GraphNeighbor){2, 3})));
        TS_ASSERT(map1.graph[2].neighbors.hasTheSameElementsAs(NodeList<GraphNeighbor>(2, (GraphNeighbor){0, 2}, (GraphNeighbor){1, 3})));
        //test for map 2
        TS_ASSERT_EQUALS(map2.graph[0].neighbors.size(), 4);
        TS_ASSERT_EQUALS(map2.graph[1].neighbors.size(), 1);
        TS_ASSERT_EQUALS(map2.graph[2].neighbors.size(), 3);
        TS_ASSERT_EQUALS(map2.graph[3].neighbors.size(), 3);
        TS_ASSERT_EQUALS(map2.graph[4].neighbors.size(), 4);
        
        TS_ASSERT(map2.graph[0].neighbors.hasTheSameElementsAs(NodeList<GraphNeighbor>(4, (GraphNeighbor){1, 1}, (GraphNeighbor){2, 2},
                                                                                             (GraphNeighbor){3, 5}, (GraphNeighbor){4, 9})));
        TS_ASSERT(map2.graph[1].neighbors.hasTheSameElementsAs(NodeList<GraphNeighbor>(1, (GraphNeighbor){0, 1})));
        TS_ASSERT(map2.graph[2].neighbors.hasTheSameElementsAs(NodeList<GraphNeighbor>(3, (GraphNeighbor){0, 2}, (GraphNeighbor){3, 3},
                                                                                            (GraphNeighbor){4, 21})));
        TS_ASSERT(map2.graph[3].neighbors.hasTheSameElementsAs(NodeList<GraphNeighbor>(3, (GraphNeighbor){0, 5}, 
                                                                                            (GraphNeighbor){2, 15}, (GraphNeighbor){4, 7})));
        TS_ASSERT(map2.graph[4].neighbors.hasTheSameElementsAs(NodeList<GraphNeighbor>(4, (GraphNeighbor){0, 13}, (GraphNeighbor){1, 3},
                                                                                            (GraphNeighbor){2, 17}, (GraphNeighbor){3, 7})));


        TS_TRACE("Ending test map constructor matrix");
    }

    void testShortestPath()
    {
        TS_TRACE("Starting test shortest path");
        
        u_int32_t graph_directional_row0[4] = 
            {
                0, 1, 5, 0
            };
        u_int32_t graph_directional_row1[4] = 
            {
                0, 0, 2, 0
            };
        u_int32_t graph_directional_row2[4] = 
            {
                0, 0, 0, 0
            };
        u_int32_t graph_directional_row3[4] = 
            {
                2, 0, 0, 0
            };
        u_int32_t* graph_directional[4] = {graph_directional_row0, graph_directional_row1, graph_directional_row2, graph_directional_row3};
        

        u_int32_t graph_nondirectional_row0[9] =
            {
                0, 0, 2, 1, 5, 0, 0, 0, 0
            };
        u_int32_t graph_nondirectional_row1[9] =
            {
                0, 0, 0, 2, 3, 0, 0, 0, 0
            };
        u_int32_t graph_nondirectional_row2[9] =
            {
                2, 0, 0, 0, 0, 0, 0, 0, 0
            };
        u_int32_t graph_nondirectional_row3[9] =
            {
                1, 2, 0, 0, 0, 4, 0, 0, 1
            };
        u_int32_t graph_nondirectional_row4[9] =
            {
                5, 3, 0, 0, 0, 0, 0, 0, 0
            };
        u_int32_t graph_nondirectional_row5[9] =
            {
                0, 0, 0, 4, 0, 0, 1, 7, 0
            };
        u_int32_t graph_nondirectional_row6[9] =
            {
                0, 0, 0, 0, 0, 1, 0, 0, 0
            };
        u_int32_t graph_nondirectional_row7[9] =
            {
                0, 0, 0, 0, 0, 7, 0, 0, 3
            };
        u_int32_t graph_nondirectional_row8[9] =
            {
                0, 0, 0, 1, 0, 0, 0, 3, 0
            };

        u_int32_t* graph_nondirectional[9] = {
            graph_nondirectional_row0,
            graph_nondirectional_row1,
            graph_nondirectional_row2,
            graph_nondirectional_row3,
            graph_nondirectional_row4,
            graph_nondirectional_row5,
            graph_nondirectional_row6,
            graph_nondirectional_row7,
            graph_nondirectional_row8,
        };

        Map directional_map(graph_directional, 4);
        Map nondirectional_map(graph_nondirectional, 9);
        
        //because of the way the NodeList's constructor is, we have to put the paths in reverse in the asserts

        //Directional map tests
        {
            NodeList<int32_t> shortPath0; directional_map.computeShortestPath(0, 2, shortPath0);
            TS_ASSERT(shortPath0.isEqualTo(NodeList<int32_t>(3, 2, 1, 0)));
            NodeList<int32_t> shortPath1; directional_map.computeShortestPath(3, 2, shortPath1);
            TS_ASSERT(shortPath1.isEqualTo(NodeList<int32_t>(4, 2, 1, 0, 3)));
            NodeList<int32_t> shortPath_null; directional_map.computeShortestPath(2, 0, shortPath_null);
            TS_ASSERT(shortPath_null.isEqualTo(NodeList<int32_t>())); 
            NodeList<int32_t> shortPath_single; directional_map.computeShortestPath(3, 0, shortPath_single);
            TS_ASSERT(shortPath_single.isEqualTo(NodeList<int32_t>(2, 0, 3))); 
        }
        //Non-Directional map tests
        {
            NodeList<int32_t> shortPath0; nondirectional_map.computeShortestPath(6, 4, shortPath0);
            TS_ASSERT(shortPath0.isEqualTo(NodeList<int32_t>(5, 4, 1, 3, 5, 6))); 
            NodeList<int32_t> shortPath1; nondirectional_map.computeShortestPath(3, 2, shortPath1);
            TS_ASSERT(shortPath1.isEqualTo(NodeList<int32_t>(3, 2, 0, 3))); 
            NodeList<int32_t> shortPath1_reversed; nondirectional_map.computeShortestPath(2, 3, shortPath1_reversed);
            TS_ASSERT(shortPath1_reversed.isEqualTo(NodeList<int32_t>(3, 3, 0, 2)));
            NodeList<int32_t> shortPath_single; nondirectional_map.computeShortestPath(0, 2, shortPath_single);
            TS_ASSERT(shortPath_single.isEqualTo(NodeList<int32_t>(2, 2, 0))); 
            NodeList<int32_t> shortPath2; nondirectional_map.computeShortestPath(4, 7, shortPath2);
            TS_ASSERT(shortPath2.isEqualTo(NodeList<int32_t>(5, 7, 8, 3, 1, 4))); 
            NodeList<int32_t> shortPath_singleton; nondirectional_map.computeShortestPath(6, 6, shortPath_singleton);
            TS_ASSERT(shortPath_singleton.isEqualTo(NodeList<int32_t>(1, 6))); 
        }
        

        TS_TRACE("Ending test shortest path");
    }

    void testMapConstructorAdjacencyList()
    {
        TS_TRACE("Starting test map constructor adjacency list");

        BasicArray adjacencyList0;
        adjacencyList0.length = 2;
        BasicArray adjacencyList0_0[2] = {BasicArray{0,nullptr}, BasicArray{0, nullptr}};
        adjacencyList0.array = adjacencyList0_0;

        BasicArray adjacencyList1;
        adjacencyList1.length = 3;
        ValuePair adjacencyList1_1[2] = {ValuePair{1, 1}, ValuePair{2, 2}};
        ValuePair adjacencyList1_2[2] = {ValuePair{0, 1}, ValuePair{2, 3}};
        ValuePair adjacencyList1_3[2] = {ValuePair{0, 2}, ValuePair{1, 3}};
        BasicArray adjacencyList1_0[3] = {BasicArray{2, adjacencyList1_1}, BasicArray{2, adjacencyList1_2}, BasicArray{2, adjacencyList1_3}};
        adjacencyList1.array = adjacencyList1_0;

        BasicArray adjacencyList2;
        adjacencyList2.length = 5;
        ValuePair adjacencyList2_1[4] = {ValuePair{1, 1}, ValuePair{2, 2}, ValuePair{3, 5}, ValuePair{4, 9}};
        ValuePair adjacencyList2_2[1] = {ValuePair{0, 1}};
        ValuePair adjacencyList2_3[3] = {ValuePair{0, 2}, ValuePair{3, 3}, ValuePair{4, 21}};
        ValuePair adjacencyList2_4[3] = {ValuePair{0, 5}, ValuePair{2, 15}, ValuePair{4, 7}};
        ValuePair adjacencyList2_5[4] = {ValuePair{0, 13}, ValuePair{1, 3}, ValuePair{2, 17}, ValuePair{3, 7}};
        BasicArray adjacencyList2_0[5] = {BasicArray{4, adjacencyList2_1}, BasicArray{1, adjacencyList2_2}, BasicArray{3, adjacencyList2_3}
            , BasicArray{3, adjacencyList2_4}, BasicArray{4, adjacencyList2_5}};
        adjacencyList2.array = adjacencyList2_0;
        
        Map map0{adjacencyList0};
        Map map1{adjacencyList1};
        Map map2{adjacencyList2};
        
        //test for map 0
        TS_ASSERT_EQUALS(map0.graph[0].neighbors.size(), 0U);
        TS_ASSERT_EQUALS(map0.graph[1].neighbors.size(), 0U);

        //test for map 1
        TS_ASSERT_EQUALS(map1.graph[0].neighbors.size(), 2);
        TS_ASSERT_EQUALS(map1.graph[1].neighbors.size(), 2);
        TS_ASSERT_EQUALS(map1.graph[2].neighbors.size(), 2);

        TS_ASSERT(map1.graph[0].neighbors.hasTheSameElementsAs(NodeList<GraphNeighbor>(2, (GraphNeighbor){1, 1}, (GraphNeighbor){2, 2})));
        TS_ASSERT(map1.graph[1].neighbors.hasTheSameElementsAs(NodeList<GraphNeighbor>(2, (GraphNeighbor){0, 1}, (GraphNeighbor){2, 3})));
        TS_ASSERT(map1.graph[2].neighbors.hasTheSameElementsAs(NodeList<GraphNeighbor>(2, (GraphNeighbor){0, 2}, (GraphNeighbor){1, 3})));
        //test for map 2
        TS_ASSERT_EQUALS(map2.graph[0].neighbors.size(), 4);
        TS_ASSERT_EQUALS(map2.graph[1].neighbors.size(), 1);
        TS_ASSERT_EQUALS(map2.graph[2].neighbors.size(), 3);
        TS_ASSERT_EQUALS(map2.graph[3].neighbors.size(), 3);
        TS_ASSERT_EQUALS(map2.graph[4].neighbors.size(), 4);
        
        TS_ASSERT(map2.graph[0].neighbors.hasTheSameElementsAs(NodeList<GraphNeighbor>(4, (GraphNeighbor){1, 1}, (GraphNeighbor){2, 2},
                                                                                             (GraphNeighbor){3, 5}, (GraphNeighbor){4, 9})));
        TS_ASSERT(map2.graph[1].neighbors.hasTheSameElementsAs(NodeList<GraphNeighbor>(1, (GraphNeighbor){0, 1})));
        TS_ASSERT(map2.graph[2].neighbors.hasTheSameElementsAs(NodeList<GraphNeighbor>(3, (GraphNeighbor){0, 2}, (GraphNeighbor){3, 3},
                                                                                            (GraphNeighbor){4, 21})));
        TS_ASSERT(map2.graph[3].neighbors.hasTheSameElementsAs(NodeList<GraphNeighbor>(3, (GraphNeighbor){0, 5}, 
                                                                                            (GraphNeighbor){2, 15}, (GraphNeighbor){4, 7})));
        TS_ASSERT(map2.graph[4].neighbors.hasTheSameElementsAs(NodeList<GraphNeighbor>(4, (GraphNeighbor){0, 13}, (GraphNeighbor){1, 3},
                                                                                            (GraphNeighbor){2, 17}, (GraphNeighbor){3, 7})));

        TS_TRACE("Ending test map constructor adjacency list");
    }

};


#endif //MAP_TESTS_HXX