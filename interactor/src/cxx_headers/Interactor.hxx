/**
 * @file Interactor.hxx
 * @author Félicien Fiscus (felicien.fgay@gmail.com)
 * @brief the Interactor C++ class that handles intermediate business logic
 * between The Entity (Database) and the Presenter and provides
 * an interface for it
 * @version 0.1
 * @date 2023-10-08
 * 
 * @copyright GNU General Public License (GPL) 2023
 * 
 */
#ifndef INTERACTOR_HPP
#define INTERACTOR_HPP
#include <stdint.h>
#include "Map.hxx"


class Interactor
{
    /**
     * @brief the building map entity that currently is loaded in the app
     * 
     */
    Map currentBuildingMap;
public:
    Interactor();
    ~Interactor();

    /**
     * @brief creates a sample map for currentBuildingMap
     * just to test it via Dart
     * 
     */
    void generateSampleMapGraph();

    /**
     * @brief creates a map for currentBuildingMap
     * from an adjacency list
     * @see Map.hxx
     * 
     */
    void generateMapGraph(BasicArray adjacencyList);


    /**
     * @brief interface method to compute the shortest path between a and b in the currentBuildingMap graph
     * 
     * @param a start point
     * @param b end point
     * @param pathSize the number of nodes that the path will be made of (including a and b)
     * @return an array of node IDs of a certain length ouputed in pathSize
     */
    int32_t* shortestPath(int32_t a, int32_t b, int32_t* pathSize);
};


#endif //INTERACTOR_HPP