#include "Node.hxx"
#include <stdarg.h>

template <typename T>
inline Node<T>::Node()
{
}


template <typename T>
inline Node<T>::Node(const T &value)
    :value(value)
{
}

template <typename T>
inline Node<T>::~Node()
{
}

template <typename T, class CompareFunction>
inline NodeList<T, CompareFunction>::NodeList()
{
}

template <typename T, class CompareFunction>
inline NodeList<T, CompareFunction>::NodeList(const T &startValue)
    :numberOfNodes(1), front(new Node<T>(startValue))
{
}

template <typename T, class CompareFunction>
inline NodeList<T, CompareFunction>::NodeList(size_t numberOfElements, ...)
{
    va_list paramList;
    va_start(paramList, numberOfElements);
    for(size_t i = 0;i<numberOfElements;++i)
    {
        pushElementFront(va_arg(paramList, T));
    }

    va_end(paramList);
}

template <typename T, class CompareFunction>
inline NodeList<T, CompareFunction>::NodeList(size_t numberOfElements, const T &value)
{
    for(size_t i = 0;i<numberOfElements;++i)
    {
        pushElementFront(value);
    }
}

template <typename T, class CompareFunction>
inline NodeList<T, CompareFunction>::~NodeList()
{
    while(front != nullptr)
    {
        delete popFront();
    }
}

template <typename T, class CompareFunction>
inline Node<T>* NodeList<T, CompareFunction>::frontNode() const
{
    return front;
}

template <typename T, class CompareFunction>
inline Node<T>* NodeList<T, CompareFunction>::lastNode() const
{
    return last;
}

template <typename T, class CompareFunction>
inline NodeList<T, CompareFunction> &NodeList<T, CompareFunction>::pushElementFront(const T &newValue)
{
    Node<T>* newNode = new Node<T>(newValue);
    newNode->next = front;
    front = newNode;
    numberOfNodes++;
    return *this;
}

template <typename T, class CompareFunction>
inline Node<T> *NodeList<T, CompareFunction>::popFront()
{
    if(size() > 0)
    {
        Node<T>* frontBuffer = front;
        front = front->next;
        frontBuffer->next = nullptr;
        if(--numberOfNodes == 0)
        {
            last = nullptr;
        }            
        return frontBuffer;
    }

    return nullptr;
}

template <typename T, class CompareFunction>
inline NodeList<T, CompareFunction> &NodeList<T, CompareFunction>::deleteNode(const T &nodeValue)
{
    Node<T>* nodeToDelete = removeNode(nodeValue);

    //removeNode already decrements the size of the list so we don't need to do that here
    if(nodeToDelete)
        delete nodeToDelete;

    return *this;
}

template <typename T, class CompareFunction>
inline Node<T> *NodeList<T, CompareFunction>::removeNode(const T &nodeValue)
{
    Node<T>* nodeToRemove = front;
    Node<T>* previousNode = nullptr;

    //search for the node to remove
    while(nodeToRemove && nodeToRemove->value != nodeValue)
    {
        previousNode = nodeToRemove;
        nodeToRemove = nodeToRemove->next;
    }

    //if we've found it
    if(nodeToRemove)
    {
        if(previousNode)
        {
            previousNode->next = nodeToRemove->next;
            if(nodeToRemove == last)
                last = previousNode;
        }
        else
            front = nodeToRemove->next;
        
        if(--numberOfNodes == 0)
            last = nullptr;
        nodeToRemove->next = nullptr;
        return nodeToRemove;
    }

    return nullptr;
}

template <typename T, class CompareFunction>
inline size_t NodeList<T, CompareFunction>::size() const
{
    return numberOfNodes;
}

template <typename T, class CompareFunction>
inline bool NodeList<T, CompareFunction>::empty() const
{
    return numberOfNodes == 0;
}

template <typename T, class CompareFunction>
inline Node<T> const* NodeList<T, CompareFunction>::findNode(const Node<T>* nodeToFind) const
{
    Node<T>* foundNode = front;
    while(foundNode && foundNode != nodeToFind) foundNode = foundNode->next;

    return foundNode;
    
}

template <typename T, class CompareFunction>
inline Node<T> *NodeList<T, CompareFunction>::findElement(const T &elem) const
{
    Node<T>* foundNode = front;
    while(foundNode && foundNode->value != elem) foundNode = foundNode->next;

    return foundNode;
}

template <typename T, class CompareFunction>
inline Node<T> *NodeList<T, CompareFunction>::findMaximum(const CompareFunction &compareFunction) const
{
    Node<T>* maxNode = frontNode();

    for(Node<T>* current = frontNode();current != nullptr;current = current->next)
    {
        if(compareFunction(maxNode->value, current->value))
        {
            maxNode = current;
        }
    }

    return maxNode;
}

template <typename T, class CompareFunction>
inline Node<T> *NodeList<T, CompareFunction>::findMinimum(const CompareFunction &compareFunction) const
{
    Node<T>* minNode = frontNode();

    for(Node<T>* current = frontNode();current != nullptr;current = current->next)
    {
        if(compareFunction(current->value, minNode->value))
        {
            minNode = current;
        }
    }

    return minNode;
}

// Function to merge two sorted linked lists
template <typename T, class CompareFunction>
Node<T>* NodeList<T, CompareFunction>::merge(Node<T>* l1, Node<T>* l2, const CompareFunction& compareFunction) {
    if (l1 == NULL) return l2;
    if (l2 == NULL) return l1;

    Node<T>* mergedList;

    if (compareFunction(l1->value, l2->value)) {
        mergedList = l1;
        mergedList->next = merge(l1->next, l2, compareFunction);
    } else {
        mergedList = l2;
        mergedList->next = merge(l1, l2->next, compareFunction);
    }

    return mergedList;
}

// Function to split a linked list into two halves
template <typename T, class CompareFunction>
void NodeList<T, CompareFunction>::split(Node<T>* source, Node<T>** front, Node<T>** back) {
    if (source == NULL || source->next == NULL) {
        *front = source;
        *back = NULL;
    } else {
        Node<T>* slow = source;
        Node<T>* fast = source->next;

        while (fast != NULL) {
            fast = fast->next;
            if (fast != NULL) {
                slow = slow->next;
                fast = fast->next;
            }
        }

        *front = source;
        *back = slow->next;
        slow->next = NULL;
    }
}

// Merge Sort for linked list
template <typename T, class CompareFunction>
void NodeList<T, CompareFunction>::mergeSort(Node<T>** headRef, const CompareFunction& compareFunction) {
    Node<T>* head = *headRef;
    Node<T>* a;
    Node<T>* b;

    if (head == NULL || head->next == NULL) {
        return; // Base case: list is empty or has only one element
    }

    // Split the list into two halves
    split(head, &a, &b);

    // Recursively sort the two halves
    mergeSort(&a, compareFunction);
    mergeSort(&b, compareFunction);

    // Merge the sorted halves
    *headRef = merge(a, b, compareFunction);
}

template <typename T, class CompareFunction>
inline NodeList<T, CompareFunction> &NodeList<T, CompareFunction>::sort(const CompareFunction& compareFunction)
{
    mergeSort(&front, compareFunction);  

    return *this;
}

template <typename T, class CompareFunction>
inline NodeList<T, CompareFunction> &NodeList<T, CompareFunction>::iterate(void (*applyFunction)(T &value))
{
    
    Node<T>* current = front;
    while(current)
    {
        applyFunction(current->value);
        current = current->next;
    } 

    return *this;
}

template <typename T, class CompareFunction>
inline bool NodeList<T, CompareFunction>::isEqualTo(const NodeList<T, CompareFunction> &otherList)
{
    
    Node<T>* current = front;
    Node<T>* otherCurrent = otherList.front;
    while(current && otherCurrent && current->value == otherCurrent->value)
    {
        current = current->next;
        otherCurrent = otherCurrent->next;
    } 

    return !current && !otherCurrent;
}

template <typename T, class CompareFunction>
inline NodeList<T, CompareFunction> &NodeList<T, CompareFunction>::concatenate(NodeList<T, CompareFunction> &otherList)
{
    if(empty())
    {
        front = otherList.front;
        last = otherList.last;
        numberOfNodes = otherList.numberOfNodes;
    }
    else if(!otherList.empty())
    {
        last->next = otherList.front;
        last = otherList.last;
        numberOfNodes += otherList.numberOfNodes;
    }

    return *this;
}

template <typename T, class CompareFunction>
inline NodeList<T, CompareFunction> NodeList<T, CompareFunction>::clone() const
{
    NodeList<T, CompareFunction> newList;
    Node<T>* newListLast = nullptr;
    Node<T>* current = front;
    if(!current)
        return newList;
    
    newList.pushElementFront(current->value);
    newListLast = newList.front;

    while((current = current->next))
    {
        newListLast->next = new Node<T>(current->value);
        newListLast = newListLast->next;
    }
    
    newList.numberOfNodes = numberOfNodes;
    newList.last = newListLast;
    return newList;

}


template <typename T, class CompareFunction>
inline bool NodeList<T, CompareFunction>::hasTheSameElementsAs(const NodeList<T, CompareFunction> &otherList)
{
    NodeList<T, CompareFunction> cloneOfThis = clone();
    NodeList<T, CompareFunction> cloneOfOther = otherList.clone();

    return cloneOfThis.sort().isEqualTo(cloneOfOther.sort());
}
