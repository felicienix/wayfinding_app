/**
 * @file Node.hxx
 * @author Félicien Fiscus (felicien.fgay@gmail.com)
 * @brief Node class and NodeList class file which implements a simple linked list
 * @version 0.1
 * @date 2023-10-08
 * 
 * @copyright GNU General Public License (GPL) 2023
 * 
 */
#ifndef NODE_HXX
#define NODE_HXX
#include "stdlib.h"

//little macro helper
#define forEachNode(nodeList, doThat) \
    {\
        auto node = nodeList.frontNode(); \
        while(node) \
        { \
            doThat(node->value); \
            node = node->next; \
        } \
    } 

/**
 * @brief function object class used to sort a NodeList of type T
 *  this is a base function object class that just returns whether a < b
 * @tparam T the type of elements to compare
 */
template<typename T>
class LessThan
{
public:

    virtual bool operator()(const T& a, const T& b) const
    {
        return a < b;
    }
};

/**
 * @brief Node class that represents the building blocks of the linked list
 * 
 * @tparam T the type of value each node holds
 */
template<typename T>
struct Node
{
    Node<T>* next = nullptr;
    T value;

    Node();
    Node(const T& value);
    ~Node();

};

/**
 * @brief Represents a simple linked list of Nodes and holds the size, front and last node of the list
 * 
 * @tparam T the type of elements to store in the list
 * @tparam CompareFunction the compare function object to use for sorting the list
 */
template<typename T, class CompareFunction = LessThan<T>>
class NodeList
{
    Node<T>* front = nullptr;
    Node<T>* last = nullptr;
    size_t numberOfNodes = 0;


public:

    NodeList();
    NodeList(const T& startValue);
    NodeList(size_t numberOfElements, ...);
    /**
     * @brief Construct a new Node List object with all values being equal to 'value'
     * 
     * @param numberOfElements
     * @param value
     */
    NodeList(size_t numberOfElements, const T& value);
    ~NodeList();

    /**
     * @brief returns the front node or nullptr if empty
     * 
     * @return Node<T>* 
     */
    Node<T>* frontNode() const;
    /**
     * @brief returns the last node or nullptr if empty
     * 
     * @return Node<T>* 
     */
    Node<T>* lastNode() const;
    /**
     * @brief creates a new node with the specified new value and then returns the updated list
     * 
     * @param newValue
     * @return NodeList<T, CompareFunction>& 
     */
    NodeList<T, CompareFunction>& pushElementFront(const T& newValue);
    /**
     * @brief remove front node and returns it
     * 
     * @return Node<T>* 
     */
    Node<T>* popFront();
    /**
     * @brief delete the first node associated with nodeValue and returns the updated list
     * 
     * @param nodeValue 
     * @return NodeList<T, CompareFunction>& 
     */
    NodeList<T, CompareFunction>& deleteNode(const T& nodeValue);
    /**
     * @brief remove the first node associated with nodeValue and returns it
     * 
     * @param nodeValue 
     * @return Node<T>* 
     */
    Node<T>* removeNode(const T& nodeValue);
    /**
     * @brief get the number of elements in the list
     * 
     * @return size_t 
     */
    size_t size() const;
    /**
     * @brief returns wether the size is 0
     * 
     */
    bool empty() const;
    /**
     * @brief finds and returns the specified node, nullptr if not found
     * 
     * @param nodeToFind 
     * @return Node<T> const* 
     */
    Node<T> const* findNode(const Node<T>* nodeToFind) const;
    /**
     * @brief finds and returns the node with the specified value, nullptr if not found
     * 
     * @param elem the value to find
     * @return Node<T>* 
     */
    Node<T>* findElement(const T& elem) const;

    /**
     * @brief finds the maximum value stored in the list according to the compareFunction
     * and returns the node holding the value or nullptr if the list is empty
     * 
     */
    Node<T>* findMaximum(const CompareFunction& compareFunction = CompareFunction{}) const;

    /**
     * @brief finds the minimum value stored in the list according to the compareFunction
     * and returns the node holding the value or nullptr if the list is empty
     * 
     */
    Node<T>* findMinimum(const CompareFunction& compareFunction = CompareFunction{}) const;

    /**
     * @brief sort the list in ascending order (using the merge sort algorithm) according to the compareFunction and returns itself
     * 
     * @param compareFunction the function to compare the values with
     * @return NodeList<T, CompareFunction>& 
     */
    NodeList<T, CompareFunction>& sort(const CompareFunction& compareFunction = CompareFunction{});
    /**
     * @brief map every element of the list using the specified applyFunction and returns itself
     * 
     * @param applyFunction 
     * @return NodeList<T, CompareFunction>& 
     */
    NodeList<T, CompareFunction>& iterate(void(*applyFunction)(T& value));
    /**
     * @brief returns wether the two lists have the same element, in the same order
     * 
     * @param otherList 
     */
    bool isEqualTo(const NodeList<T, CompareFunction>& otherList);

    /**
     * @brief concatenates this list with otherList, forming the "thisOtherList" list and returning it
     * 
     * @param otherList 
     * @return NodeList<T, CompareFunction>& 
     */
    NodeList<T, CompareFunction>& concatenate(NodeList<T, CompareFunction>& otherList);
    
    /**
     * @brief clones the list, should be used to make a copy of a list 
     * instead of using '=' as it only copies the pointers and not the values
     * 
     * @return NodeList<T, CompareFunction> 
     */
    NodeList<T, CompareFunction> clone() const;
    /**
     * @brief returns whether this list has the same elements as the otherList no matter 
     * the order the elements are in
     * 
     * @param otherList 
     */
    bool hasTheSameElementsAs(const NodeList<T, CompareFunction>& otherList);

private:
    //sort helper methods
    void split(Node<T>* source, Node<T>** front, Node<T>** back);
    Node<T>* merge(Node<T>* l1, Node<T>* l2, const CompareFunction& compareFunction);
    void mergeSort(Node<T>** headRef, const CompareFunction& compareFunction);
};

#include "Node.inl"


#endif //NODE_HXX