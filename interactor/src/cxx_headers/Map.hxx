/**
 * @file Map.hxx
 * @author Félicien Fiscus (felicien.fgay@gmail.com)
 * @brief File for the Map object class that represents a graph of nodes,
 * here a graph is represented by an adjacency list (or array)
 * with each GraphNode of the array having a list of GraphNeighbor
 * @version 0.1
 * @date 2023-10-08
 * 
 * @copyright GNU General Public License (GPL) 2023
 * 
 */
#ifndef MAP_HXX
#define MAP_HXX
#include "Node.hxx"

/**
 * @brief struct representing the link to a neighbor of a GraphNode
 */
struct GraphNeighbor
{
    int32_t neighborID = 0;
    u_int32_t weight = 0;

    GraphNeighbor()
    {}
    GraphNeighbor(int32_t id, u_int32_t _weight)
        :neighborID(id), weight(_weight){}

    bool operator<(const GraphNeighbor& neighbor) const
    {
        return neighborID < neighbor.neighborID;
    }
    bool operator==(const GraphNeighbor& neighbor) const
    {
        return neighborID == neighbor.neighborID;
    }
};

/**
 * @brief A node in a graph represented by its ID (i.e value)
 * and its neighbors
 * 
 */
class GraphNode
{
public:
    GraphNode();
    GraphNode(int32_t valueID);
    ~GraphNode();
    
    int32_t nodeID;
    NodeList<GraphNeighbor> neighbors;
    /** only used for the A* pathfinding algorithm **/
    float localGoal;
    float globalGoal;
    int32_t parentNode = 0;
    /***********************************************/

    bool operator<(const GraphNode& otherNode) const
    {
        return nodeID < otherNode.nodeID;
    }

};

/**
 * @brief Map object class represented by a graph using a GraphNode array
 * 
 */
class Map
{
    /**
     * @brief number of nodes in the graph
     * 
     */
    size_t graphSize;
public:

    GraphNode* graph = nullptr;
    /**
     * @brief Construct a new Map object with an empty graph (graph == nullptr)
     * 
     */
    Map();
    /**
     * @brief Construct a new Map object with 'matrixSize' nodes labeled 0,1, .., 'matrixSize'
     *  and the graph's edges are specified by the adjacencyMatrix
     * @param adjacencyMatrix positive weights only, need to be a square matrix
     * @param matrixSize
     */
    Map(u_int32_t **adjacencyMatrix, size_t matrixSize);
    /**
     * @brief construct a new map object with and adjacency list as input,
     * the adjacency list is an array of neighbor arrays with the size of the first array being the total number of nodes.
     * The pointer of BasicArray needs to point to a array of BasicArray (so needs to be of type BasicArray*)
     * and each BasicArray needs to be a array of ValuePair(one for nodeID the other for the weight) (so of type ValuePair*) 
     * @see InteractorWrapper.h
     * 
     * @param adjacencyList 
     */
    Map(struct BasicArray adjacencyList);
    ~Map();

    size_t getNumberOfNodes() const;

    /**
     * @brief constructs the graph after the instance's constructor
     * 
     * @param adjacencyMatrix positive weights only, need to be a square matrix
     * @param matrixSize 
     */
    void buildGraph(u_int32_t **adjacencyMatrix, size_t matrixSize);
    /**
     * @brief construct the graph with and adjacency list as input,
     * the adjacency list is an array of neighbor arrays with the size of the first array being the total number of nodes.
     * The pointer of BasicArray needs to point to a array of BasicArray (so needs to be of type BasicArray*)
     * and each BasicArray needs to be a array of ValuePair(one for nodeID the other for the weight) (so of type ValuePair*) 
     * @see InteractorWrapper.h
     * 
     * @param adjacencyList 
     */
    void buildGraph(BasicArray adjacencyList);

    /**
     * @brief use A* algorithm to find best path  between two points in the graph (start and end)
     *  and returns the path found in finalPath variable.
     *  Implemented using the wikipedia article : https://en.wikipedia.org/wiki/A*_search_algorithm
     * @param start the start node in the graph
     * @param end the end node in the graph
     * @param finalPath holds the shortest path or is left unchanged if no path has been found
     */
    void computeShortestPath(int32_t start, int32_t end, NodeList<int32_t>& finalPath);

private:
    /**
     * @brief custom function object class to sort the list of node IDs via their global goal
     * 
     */
    class GraphLessThan : public LessThan<int32_t>
    {
        GraphNode** graphReference = nullptr;
    public:
        explicit GraphLessThan(GraphNode** _graph)
            :graphReference(_graph){}
        
        bool operator()(const int32_t& a, const int32_t& b) const override
        {
            return (*graphReference)[a].globalGoal < (*graphReference)[b].globalGoal;
        }
    };
    GraphLessThan graphCompareFunction = GraphLessThan(&graph);
};


#endif //MAP_HXX